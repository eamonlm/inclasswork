#include <iostream>
#include <fstream>
#include <string>
#include "DoubleHash.h"
using namespace std;

/********************************************
 * Function Name  : findAllPairs
 * Pre-conditions : int, string
 * Post-conditions: 
 * This function prints out all pairs of integers that add up to a specific value  * in a given text file 
 ********************************************/
void findAllPairs(int desiredValue, string fileName);
/********************************************
 * Function Name  : main
 * Pre-conditions :
 * Post-conditions: int
 *
 ********************************************/
int main(){

  // gets file name
  cout << "Please enter the name of the file to read in (include file extension: " << endl;
  string fileName;
  cin >> fileName;
  
  cin.clear();
  // gets int value to search for
  cout << "Please enter the desired value to search for: " << endl;
  int desiredValue;
  cin >> desiredValue;
  
  findAllPairs(desiredValue, fileName);
  
  return 0;
}

void findAllPairs(int desiredValue, string fileName){
  ifstream ifs;
  ifs.open(fileName);
  // HashTable
  HashTable<int> h1(100);
   // temporary int variable
  int temp = 0;
  // other number that must exist in array in order to have a pair that add up to desiredValue
  int otherRequiredNum = 0;

  // inserts integers from file into HashTable
  while(ifs >> temp){
    h1.insert(temp);
  }

  // loops through HashTable to find pairs O(n) runtime
  for(int i = 0; i < desiredValue; i++){
    otherRequiredNum = desiredValue - i;
    if(h1.contains(i) && h1.contains(otherRequiredNum)){
      cout << i << " and " << otherRequiredNum << " both add up to " << desiredValue << " and exist in the given file!" << endl;
    }
  }
}
