/**********************************************
* File: AVLAuthorTest.cpp
* Author: 
* Email: 
*  
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

// Struct goes here

struct Author {
  std::string firstName;
  std::string lastName;
  
  Author(std::string firstName, std::string lastName): firstName(firstName), lastName(lastName) {}

  bool operator<(const Author & rhs) const {
    
    if(lastName < rhs.lastName){
      return true;
    }
    else if(lastName == rhs.lastName){
      if(firstName < rhs.firstName){
	return true;
      }
    }
    
    return false;
}

  bool operator==(const Author& rhs) const {
    if(lastName != rhs.lastName){
      return false; 
    }
    else{
      if(firstName != rhs.firstName){
	return false;
      }
    }
    return true;
  }

  friend std:: ostream& operator <<(std::ostream& outStream, const Author& printAuth);

};

std:: ostream& operator <<(std::ostream& outStream, const Author& printAuth){
  outStream << printAuth.lastName << ", " << printAuth.firstName;
  return outStream;
}


/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{
  AVLTree <Author> avlAuthor;
  
  Author authors[5] = { {"Frank", "Herbert"}, {"John", "Tolkien"}, {"George", "Martin"}, {"Dean", "Martin"}, {"Patrick", "Rothfuss"}};

  for(int i = 0; i < 5; i++){
    avlAuthor.insert(authors[i]);
  }

  avlAuthor.printTree();

  avlAuthor.remove(authors[3]);

  std::cout << "--------" << std::endl;

  avlAuthor.printTree();
}
