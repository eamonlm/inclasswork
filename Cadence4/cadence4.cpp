#include<iostream>
#include<vector>
#include<cstring>
#include<fstream>
#include<locale> 
#include<algorithm>

using namespace std;

int main(int argc, char ** argv){
  
  vector<string> dictSubset;
  string temp;
  ifstream dict;

  dict.open("smalldict.txt");
  // gets dictionary into vector of strings
  while(dict >> temp){
    dictSubset.push_back(temp);
  }

  // sorts dictionary lexicographically (using lambda)
  sort(dictSubset.begin(), dictSubset.end(), [](const string &a, const string &b) { return a.size() < b.size(); });

  // gets license plate info
  cout << "What license plate do you want to search for?" << endl;
  cin.clear();
  
  // stores license plate info
  string licensePlate;
  cin >> licensePlate;

  // gets letters from license plate, places them into their own string
  string alphaLicensePlate;
  for(int i = 0; i < licensePlate.length(); i++){
    if(isalpha(licensePlate[i])){
      alphaLicensePlate.push_back(licensePlate[i]);
    }
  }

  // stores whether license plate is contained within given word
  bool contains = true;

  // iterates through dictionary
  for(int i = 0; i < dictSubset.size(); i++){
    // resets whether is contained or not
    contains = true;
    // checks if current dictionary entry is at least as large as the license plate letters
    if(dictSubset[i].length() >= alphaLicensePlate.length()){
      // goes through each letter from license plate
      for(int j = 0; j < alphaLicensePlate.length(); j++){
	// if letter is not in the word, breaks out of for loop, sets match to false
	if(dictSubset[i].find(alphaLicensePlate[j]) == -1){
	  contains = false;
	  break;
	}
      }
      // exits if match is found 
      if(contains){
	cout << "Match for: '" << alphaLicensePlate << "' is " << dictSubset[i] << endl;
	break;
      }
    }
  }

  return 0;

}
