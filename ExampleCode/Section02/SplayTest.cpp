/****************************************
 * File name: SplayTest.h  
 * Author: Matthew Morrison 
 * Contact E-mail: matt.morrison@nd.edu 
 * 
 * This file contains the main driver function
 * for the BST Code presented in Lecture
 * *************************************/

#include <iostream>
#include "SplayTree.h"

/************************************
 * Function Name: main  
 * Preconditions: int, char **
 * Postconditions: new object   
 * Creates a new SplayTree Node given an element and two 
 * nodes for left and right.
 * *********************************/
int main(int argc, char **argv)
{
    

    SplayTree<int>* theBST = new SplayTree<int>();
    SplayTree<int> copyBST;
    
    theBST->insert(25);
    theBST->insert(10);
    theBST->insert(15);
    theBST->insert(18);
    theBST->insert(31);
    theBST->insert(26);
    
    // Test Assignment Operator Constructor
    copyBST = *theBST;
    std::cout << "Assignment Operator: copyBST = *theBST;" << std::endl;
    std::cout << "*theBST elements: " << *theBST << std::endl;
    std::cout << "copyBST elements: " << copyBST << std::endl;
    std::cout << "theBST->findMin():" << theBST->findMin() << std::endl;
    std::cout << "copyBST.findMax():" << copyBST.findMax() << std::endl<< std::endl;
 
    std::cout << "Insert Test: theBST->insert(26); and copyBST.insert(1);" << std::endl;
    theBST->insert(26);
    copyBST.insert(1);
    std::cout << "theBST elements : " << *theBST << std::endl;
    std::cout << "copyBST elements: " << copyBST << std::endl << std::endl;
    
    std::cout << "remove Test: theBST->remove(18); and copyBST.remove(10);" << std::endl;
    theBST->remove(18);
    copyBST.remove(10);
    std::cout << "theBST elements : " << *theBST << std::endl;
    std::cout << "copyBST elements: " << copyBST << std::endl << std::endl;
    
    // Test Move Constructor 
    std::cout << "Move Constructor Test: BST<int> moveBST(std::move(copyBST));" << std::endl;
    SplayTree<int> moveBST(std::move(copyBST));
    std::cout << "moveBST elements: " << moveBST << std::endl;
    std::cout << "copyBST elements: " << copyBST << std::endl;
 
    // Insert Test after Move   
    std::cout << "Insert Test: theBST->insert(26);" << std::endl;
    moveBST.insert(45);
    std::cout << "moveBST elements: " << moveBST << std::endl;

    return 0;
}
