/**********************************************
* File: BTreeWeeklyAssignment.cpp
* Author: Eamon Lopez Marmion
* Email: elopez7@nd.edu
*
* This is the driver function for your in-class
* assignment
*
* Must push the following files to GitLab
* BTreeInClass.cpp
* BTree.h - modified with your
* BTreeNode.h - modified with your
* used g++ -g -std=c++11 -Wpedantic BTreeWeeklyAssignment.cpp -o BTreeWeeklyAssignment to compile
**********************************************/

#include "BTree.h"
#include <string>
#include <stdlib.h>

// Data Struct

struct Data {
  int data;
  int key;
  Data(): key(0), data(0) {}
  Data(int key, int data): key(key), data(data) {}

  bool operator< (const Data& d) const {
    return key < d.key;
  }

  bool operator> (const Data& d) const {
    return key > d.key;
  }

  bool operator== (const Data& d) const {
    return key == d.key;
  }

  Data& operator= (const Data&  d) {
    data = d.data;
    key = d.key;
    return *this;
  }
  friend std:: ostream& operator <<(std::ostream& outStream, const Data& printData);

};

std:: ostream& operator <<(std::ostream& outStream, const Data& printData){
  outStream << printData.key << ": " << printData.data;
  return outStream;
}


/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*  
* This is the main driver function for the program
********************************************/
int main(int argc, char** argv){

  // Initial test code
  BTree<Data> database(3);
  
  // store temp ints to generate structs
  int tempKey, tempData;
  Data tempStruct;

  srand(time(0));
  // populates database
  for(int i = 0; i < 1000; i++){
    // generates random variables
    tempKey = rand() % 1000;
    tempData = rand() % 1000;

    // creates temp data struct, inserts it into database
    tempStruct = Data(tempKey, tempData);
    database.insert(tempStruct);
  }

  // creates 10 random data structs, print their value if they are within the database
  for(int i = 0; i < 10; i++){
    // generates random variables
    tempKey = rand() % 1000;

    // data can be equal to 0, as the only thing printFoundNode compares is the key when checking equality
    // creates temp data struct, inserts it
    tempStruct = Data(tempKey, 0);
    database.printFoundNode(tempStruct);    
  }
  
  return 0;
}
